var app = angular.module('myApp', [])
  .controller('myController', ['$scope', function ($scope) {
    $scope.isActivelyLooking = true;
    $scope.projectList = {
      mainProject: {
        title: 'Notify',
        img: 'assets/images/projects/notify.jpg',
        url: 'https://www.geodecisions.com/notify',
        summary: {
          modalId: 'summary',
          short: 'Notify is a map driven mass notification application. Built on the GeoQ platform, using Esri\'s JS mapping api.',
          detail: [
            'Our clients are able to use Notify to send automated messages to all or any subset of their contacts. The application allows the user to draw an area on a map to select which customers they wish to send notifications to.',
            'Notify supports 4 different contact methods, SMS, Voice using Text To Speech, Voice using recorded message, and/or Email. ' +
            'Some of our typical clients are water companies who need to contact their customers about water quality issues, boil water advisories, or billing notifications. ' +
            'Other use cases are things like Corporate Security, where a hurricane is coming in to shore and the company wants to ensure that all of their employees in that area are safe, or if they need help.'
          ]
        },
        content: [
          {
            modalId: 'n1',
            short: 'Refactored the entire application to make proper use of Bootstrap responsive classes',
            detail: [
              'One of the largest tasks I undertook when I started on this project was to rewrite the entire CSS/HTML base of the application, to allow us to more easily expand the sidebar overtop of the map area for a workflow that required more text, and less map.'
            ]
          },
          {
            modalId: 'n2',
            short: 'Wrote a vital client-side caching system to improve performance',
            detail: [
              'I created a simple caching system to increase performance, and reduce the number of unneeded requests for data that we already loaded. It also helped improve the flow of the website, by making it so that going back and forth between tabs didn\'t take forever due to slow loading times.'
            ]
          },
          {
            modalId: 'n3',
            short: 'Lead developer on the Corporate Security module',
            detail: [
              'The most recent task I worked on, was designing an entire system from the ground up for a new Module within the application that we named the "Safety Module" which allows a corporation to send out mass SMS messages(these are called "Safety Events") to users in a specific area (selected from a map).' +
              ' The contacts that got those SMS messages were able to respond, any number of times, and their responses would change their status within that "Safety Event." The corporation could then view contacts that replied in a negative status(among other statuses), and respond to them as a group, or individually' +
              ' to see if they need any assistance. (Contact me for further details on this topic)'
            ]
          },
        ]
      },
      projects: [
        {
          title: 'Discord.ts/Discordx',
          img: 'assets/images/projects/discordts.jpg',
          url: 'https://github.com/oceanroleplay/discord.ts',
          summary: {
            modalId: 't-sum',
            short: 'Discordx is a typescript framework for creating discord bots, built on top of discord.js.'
          },
          content: [
            {
              modalId: 't1',
              short: 'Bug fixes',
              detail: ['The library had a bug that was wrongly casting a string to an unsafe integer. I wrote the code that fixed it.']
            },
            {
              modalId: 't2',
              short: 'Migrate to a new github project',
              detail: ['The creator of the original library has a reputation for going MIA randomly, so myself and another developer decided to create Discordx to ensure developers will still have access to an easy to install library, with development and stable releases.']
            },
            {
              modalId: 't3',
              short: 'Support other developers',
              detail: ['I provide support to other developers who are having issues using the library.']
            },
          ]
        },
        {
          title: 'Track',
          img: 'assets/images/projects/bristow-track.jpg',
          url: 'https://www.geodecisions.com/track',
          summary: {
            modalId: 't-sum',
            short: 'Track is a map driven asset tracking application. Built on the GeoQ platform, using Esri\'s js mapping api.'
          },
          content: [
            {
              modalId: 't1',
              short: 'Created a query builder UI',
              detail: ['Query builder UI allows users to query nearby assets using any number of conditions']
            },
            {
              modalId: 't2',
              short: 'Implemented simple client-side pagination',
              detail: ['While I was fixing a few bugs, I noticed that the UI was having trouble rendering all the data properly, which was a source of some of the bugs. Implementing this pagination system fixed those.']
            },
          ]
        },
        {
          title: 'EFlight GT',
          img: 'assets/images/projects/eflightgt.jpg',
          url: 'https://www.geodecisions.com/track',
          summary: {
            modalId: 'efgt-sum',
            short: 'EFlight GT is a set of custom applications that works along side the Track Application that deals with air traffic control of helicopters.',
          },
          content: [
            {
              modalId: 'efgt-1',
              short: 'Designed & Implemented full front-end redesign',
              detail: ['For this project, I suggested a few UI redesigns in order to make the UI more performant for the client\'s needs. By reducing the amount of unneeded data being displayed, I was able to make the operations of our client much more efficient, and the client helped with the design along the way.',
                'The application displays many tables of data(5+), each row could be clicked to go to the details belonging to that row, The previous design had all of these tables in 1 column, making it incredibly difficult to view the tables at the bottom of the screen.' +
                ' It also displayed ALL of the data that was being sent back to us, even though most of that data was not required on this screen. The redesigned version started by reducing the amount of unneeded data displayed, and because of this data reduction we were able to fit the grids into 2 columns, so that all of the grids were able to be viewed at once.' +
                'This application went from requiring an entire 1920x1080 monitor in a portrait orientation in order to see all the data, to now only requiring half of that same monitor, saving time and screenspace for the operators, while also making air traffic safer due to improved operator efficiency.',
                'This is one of my proudest UI tasks that I have ever done. Not because it was a difficult task, but because of how incredibly greatful the client was for such a simple redesign. They were thrilled with the improvements to application performance and operator efficiency after my redesign was deployed.',
                '(Contact me if you would like screenshots & even more details of this example, I love going into detail on the minutia of UI design.)']
            },
          ]
        },
        {
          title: 'GeoQ',
          img: 'assets/images/projects/geo.png',
          noImgBorder: true,
          summary: {
            modalId: 'gq-sum',
            short: 'GeoQ is the backend system for Notify & Track. It is built on Node.js, and RabbitMQ using a micro-service architecture.'
          },
          content: [
            {
              modalId: 'gq-1',
              short: 'Implemented Callfire Postbacks for SMS responses (among many other features)'
            },
          ]
        },
        {
          title: "Costbooks",
          img: 'assets/images/projects/geo.png',
          noImgBorder: true,
          summary: {
            modalId: 'c-sum',
            short: 'An internal application that helps Project Managers and others determine how profitable the organization is. '
          },
          content: [
            {
              modalId: 'c-1',
              short: 'Created an entire costbooking application from scratch, based only on an Excel spreadsheet and management/user input',
              detail: [
                "I was the one and only developer on this project, and I got the app to a usable state over a single summer. " +
                "This app was built using .Net Core for the backend, with windows authentication, and the front end was created using Angular 2. " +
                "On this project, I had to ensure the site performed efficiently, and that the UI operated in a way that felt natural to the users. " +
                "With the help of those that would be using it, I made sure that the UI and all of the components worked well together. " +
                "The application and all of its formula were based on an old excel sheet that had a large amount of inter-connected pages and formulas."
              ]
            },
          ]
        },
        {
          title: "Project Health and Review Module (PHaRM)",
          img: 'assets/images/projects/geo.png',
          noImgBorder: true,
          summary: {
            modalId: 'pharm-sum',
            short: 'An internal application that helps Project Managers and stakeholders determine how well their projects are doing based on multiple statistics. ',
            detail: ''
          },
          content: [
            {
              modalId: 'pharm-1',
              short: 'Responsive, Mobile Design & Implementation',
              detail: [
                "One of the things i'm most proud of on this project was how I incorporated to mobile support requirement of this application. " +
                "I did this by making the navbar change it's location based on window size. When viewed on a large screen it is a vertical navbar fixed to the side. " +
                "On a small screen, the navbar is horizontal, fixed at the top, with icons and text. As the window gets even smaller the text of the navbar disappears and only the icons remain. " +
                "In all cases the navbar is able to be hidden, but the behavior differs very slightly based on screen size."
              ]
            },
          ]
        },
        {
          title: "Resource Planner",
          img: 'assets/images/projects/geo.png',
          noImgBorder: true,
          summary: {
            modalId: 'rp-sum',
            short: 'Internal application used to track allocated hours of employees, with reporting features'
          },
          content: [
            {
              modalId: 'rp-1',
              short: 'Fixed a critical bug that was costing the company time & money',
              detail: [
                "I was first tasked with fixing a critical bug, which over developers were not able to resolve. " +
                "Later on, I was tasked with adding new features, like a nice dashboard that shows stats, the user's managed projects, with links, " +
                "and other helpful usability features for the managers to make the use of the site as intuitive and easy to use as possible. " +
                "While working on this project, I also had to fork some Github repositories, and modify them to fit our needs, or fix errors that were not properly tested.",
                "Our team won the Presidential Quality award for our work on Resource Planner, and ResourcePlanner is now in use across the entire company of Gannett Fleming (2500+ employees)"
              ]
            },
          ]
        },
        {
          title: "CubicPvP",
          desc: ["Custom coded minecraft server based on the theme of Assassin's Creed"],
          img: "assets/images/projects/mcserver.jpg",
          links: [{
            url: "http://cubicpvp.net",
            urlWords: "Visit CubicPvP Forums"
          }],
          content: [
            {
              modalId: 'cp-1',
              short: 'Creating multiple custom addons to add features that are not present in normal minecraft, that enhance the player experience.',
              detail: ['']
            }
          ]
        },
        {
          title: "Above & Beyond English Setter Rescue",
          desc: [""],
          img: "assets/images/projects/esrescue.jpg",
          links: [{
            url: "http://esrescue.org",
            urlWords: "Visit ESRescue"
          }],
          content: [
            {
              modalId: 'e-1',
              short: 'Web Admin & Developer',
              detail: [
                'I worked on setting up the website so that the organization members can easily add and manage the dogs that they have up for adoption. Within a month of going live we had all but one dog that was pending adoption!'
              ]
            }
          ]
        },
      ]
    };
    /**
     * 
     * EXPERIENCE SECTION * 
     * 
     */
    $scope.experience = [
      {
        title: 'Fulfillment Specialist',
        place: 'Chewy EFC3',
        startYear: 'July 2021',
        desc: [
          'I follow the instructions of a fulfillment system that tells me where to grab an item, and which box to place the item in.',
          'I increased my efficiency by creating custom, double sided barcode sheets that make it easier and faster to do my job.'
        ]
      },
      {
        title: 'Contributor',
        place: 'Discord.ts/Discordx',
        placeLink: 'https://github.com/oceanroleplay/discord.ts',
        startYear: '2020',
        desc: [
          'Helping to provide support and maintenance for a typescript decorator framework built on top of Discord.js',
        ]
      },
      {
        title: 'Owner',
        place: 'CubicPvP',
        startYear: '2013',
        // desc: [
        //   'I was contacted by an educational aid who worked at the Vo-Tech and who occasionally came into our class to assist students. ' +
        //   'She was talking to us about all of the different dogs in the dog shelter where she got her dog from, and we ended up looking at the dog shelter site. ',
        //   'I looked at the site and it was old, buggy and very confusing to navigate. I instantly volunteered to remake their website. I managed to get permission from my Vo-Tech teacher to start working on all of this during my ITO days, every friday. ' +
        //   'I set up their website with WordPress and made it easy for them to modify the data. ' +
        //   'Some tasks I had to perform later on included adding SSL organization level security to the site, and making the site more responsive by turning on content compression.'
        // ]
        desc: [
          'Started the server in 2013, closed in 2018, and recently made the announcement that I am rewriting the server from scratch.',
          'Manage & deploy the server\'s website',
          'Custom coded plugins to modify the gameplay of the server.',
          'Manage SQL databases, server staff and user issues all while consistently adding new features to the server.'
        ]
      },
      {
        title: 'Application Developer',
        place: 'GeoDecisions',
        placeLink: 'http://www.geodecisions.com/',
        startYear: '2018',
        endYear: '2020',
        // desc: [
        //   'Many of the projects above are related to my work at Geodecisions.' +
        //   'After my internship was over, I was hired on full time and moved from internal projects to the products team, and began working on the GeoQ platform.'
        // ]
        desc: [
          // notify & track
          'Made numerous improvements to performance by implementing client-side caching logic, pagination, and more.',
          // notify & also eflight GT
          'Refactored entire front-end HTML and CSS to customize Bootstrap and use responsive design standards.',
          // notify contacts count footer
          'Removed code duplication & UI inconsistencies by creating a shared component that could be used across the entire application.',
          // safety module, sms responses, adding logrocket to multiple applications
          'Led development on multiple tasks.',
          // notify & locate
          'Analyzed user behavior using LogRocket to find areas where the application could be improved.',
          // eflight GT
          'Involved client in live design demos, allowing us to iterate on different UI/UX options more quickly than we could have otherwise.' + ' ' +
          'This resulted in an immeasurably improved UI/UX that the client was exceedingly delighted by, because they were involved during the design discussion.',
          // track
          // 'Created a custom query builder interface so that users could find nearby assets by specific criteria.'
        ]
      },
      {
        title: 'Intern',
        place: 'GeoDecisions',
        placeLink: 'http://www.geodecisions.com/',
        startYear: '2015',
        endYear: '2018',
        desc: [
          'Fixed a critical bug in a resource planning and allocation application which was inconveniencing the project managers on a daily basis.',

          'Developed an incredible mobile responsive design for a project health reporting application.',

          'Designed and developed an entire project budgeting and reporting application from scratch, based only on an excel spreadsheet.'
        ]
      },
      {
        title: 'Volunteer Web Developer & Site Admin',
        place: 'Above & Beyond English Setter Rescue',
        placeLink: 'https://esrescue.org',
        startYear: '2014',
        // desc: [
        //   'I was contacted by an educational aid who worked at the Vo-Tech and who occasionally came into our class to assist students. ' +
        //   'She was talking to us about all of the different dogs in the dog shelter where she got her dog from, and we ended up looking at the dog shelter site. ',
        //   'I looked at the site and it was old, buggy and very confusing to navigate. I instantly volunteered to remake their website. I managed to get permission from my Vo-Tech teacher to start working on all of this during my ITO days, every friday. ' +
        //   'I set up their website with WordPress and made it easy for them to modify the data. ' +
        //   'Some tasks I had to perform later on included adding SSL organization level security to the site, and making the site more responsive by turning on content compression.'
        // ]
        desc: [
          'Moved the organization from a broken Yahoo Pages site to WordPress.',
          'The organization is now the #1 research result when looking up how to adopt english setters on Google, due to greatly improved SEO from the switch to WordPress.',
          'Saved the organization money by finding a more cost effective web hosting company.',
          'Configured WordPress so it is more intuitive for the organization members to create and modify pages.',
          'Setup and manage hosting, SSL certificates & Google Suite for the organization.'
        ]
      }
    ];

    /********************
     * 
     * SKILLS SECTION
     * 
     ********************/
    $scope.skills = [
      {
        title: 'Full-Stack Software Development',
        label: 'Excellent',
        level: '100%'
      },
      {
        title: 'Innovative UI/UX Design & Development',
        label: 'Excellent',
        level: '100%'
      },
      {
        title: 'JavaScript, TypeScript',
        label: 'Excellent',
        level: '100%'
      },
      {
        title: 'HTML, Responsive CSS, Bootstrap',
        label: 'Excellent',
        level: '100%'
      },
      {
        title: 'MongoDB, Express, Angular, Node.JS (MEAN Stack)',
        label: 'Excellent',
        level: '100%'
      },
      {
        title: 'GIS Mapping Frameworks & Services',
        label: 'Very Good',
        labelTitle: 'Have worked extensively with Esri, but have occasionally worked with other mapping frameworks as well.',
        level: '90%'
      },
      {
        title: 'Testing Frameworks (Mocha, Chai)',
        label: 'Very Good',
        level: '90%'
      },
      {
        title: 'C#, .NET, .NET Core',
        label: 'Very Good',
        labelTitle: 'Very experienced with these languages.',
        level: '90%'
      },
      {
        title: 'Python, RabbitMQ',
        label: 'Good',
        labelTitle: 'I have written multiple statistical analysis\' in Python',
        level: '75%'
      },
      {
        title: 'SQL, MySQL',
        label: 'Good',
        level: '60%'
      }
    ];
  }])
  .config([
    '$compileProvider',
    function ($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|mumble):/);
      // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
  ]);
